import { Pipe, PipeTransform } from '@angular/core';
import { FsService } from './fs.service';

@Pipe({
  name: 'getItemName'
})
export class GetItemNamePipe implements PipeTransform {

  constructor(private fs: FsService) { }

  itemDesc = 'ITEM NOT FOUND';

  transform(id: string): string {
    this.fs.getItem(id).subscribe(
      data => {
        this.itemDesc = data.desc;
        return this.itemDesc;
      },
      error => {
        return 'ERROR';
      });
    return 'ITEM NOT FOUND';
  }

}
