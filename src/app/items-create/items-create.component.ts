import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { FsService } from '../fs.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';

@Component({
  selector: 'app-items-create',
  templateUrl: './items-create.component.html',
  styleUrls: ['./items-create.component.css']
})
export class ItemsCreateComponent implements OnInit {

  sellers;

  itemsForm: FormGroup;

  constructor(private router: Router, private fs: FsService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.sellers = this.fs.getSellersJson();
    this.itemsForm = this.formBuilder.group({
      'sellerId': [null, Validators.required],
      'desc': [null, Validators.required],
      'qoh': [null, Validators.required],
      'buy': [null, Validators.required],
      'sell': [null, Validators.required],
      'ship': [null, Validators.required],
      'tax': [null, Validators.required],
    });
  }

  onFormSubmit(form: NgForm) {
    this.fs.postItems(form)
      .subscribe(res => {
        const id = res['key'];
        this.router.navigate(['/items-details', id]);
      }, (err) => {
        console.log(err);
      });
  }

}
