import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { FsService } from '../fs.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-sellers-create',
  templateUrl: './sellers-create.component.html',
  styleUrls: ['./sellers-create.component.css']
})
export class SellersCreateComponent implements OnInit {

  sellersForm: FormGroup;

  constructor(private router: Router, private fs: FsService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.sellersForm = this.formBuilder.group({
      'name': [null, Validators.required],
      'email': [null, Validators.required],
      'phone': [null, Validators.required],
      'address': [null, Validators.required],
      'city': [null, Validators.required],
      'state': [null, Validators.required],
      'zip': [null, Validators.required],
      'rating': [null, Validators.required]
    });
  }

  onFormSubmit(form: NgForm) {
    this.fs.postSellers(form)
      .subscribe(res => {
        const id = res['key'];
        this.router.navigate(['/sellers-details', id]);
      }, (err) => {
        console.log(err);
      });
  }

}
