import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { FsService } from '../fs.service';

@Component({
  selector: 'app-photos-detail',
  templateUrl: './photos-detail.component.html',
  styleUrls: ['./photos-detail.component.css']
})
export class PhotosDetailComponent implements OnInit {

  photo = {};

  itemName = 'ITEM DELETED';

  constructor(private route: ActivatedRoute, private router: Router, private fs: FsService) { }

  ngOnInit() {
    this.getPhotoDetails(this.route.snapshot.params['id']);
  }

  getPhotoDetails(id) {
    this.fs.getPhoto(id)
      .subscribe(data => {
        this.photo = data;
        this.fs.getItem(data['itemId']).subscribe(dataa => {
          this.itemName = dataa.desc;
        });
      });
  }

  deletePhoto(id) {
    this.fs.deletePhotos(id)
      .subscribe(res => {
        this.router.navigate(['/photos']);
      }, (err) => {
        console.log(err);
      }
      );
  }

}

