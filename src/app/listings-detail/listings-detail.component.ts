import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { FsService } from '../fs.service';

@Component({
  selector: 'app-listings-detail',
  templateUrl: './listings-detail.component.html',
  styleUrls: ['./listings-detail.component.css']
})
export class ListingsDetailComponent implements OnInit {

  listing = {};

  itemName = 'ITEM DELETED';

  vendorName = 'VENDOR DELETED';

  constructor(private route: ActivatedRoute, private router: Router, private fs: FsService) { }

  ngOnInit() {
    this.getListingDetails(this.route.snapshot.params['id']);
  }

  getListingDetails(id) {
    this.fs.getListing(id)
      .subscribe(data => {
        this.listing = data;
        this.fs.getItem(data['itemId']).subscribe(dataa => {
          this.itemName = dataa.desc;
        });
        this.fs.getVendor(data['vendorId']).subscribe(dataaa => {
          this.vendorName = dataaa.name;
        });
      });
  }

  deleteListing(id) {
    this.fs.deleteListings(id)
      .subscribe(res => {
        this.router.navigate(['/listings']);
      }, (err) => {
        console.log(err);
      }
      );
  }

}

