import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { FsService } from '../fs.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';

@Component({
  selector: 'app-photos-create',
  templateUrl: './photos-create.component.html',
  styleUrls: ['./photos-create.component.css']
})
export class PhotosCreateComponent implements OnInit {

  items;

  photosForm: FormGroup;

  constructor(private router: Router, private fs: FsService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.items = this.fs.getItemsJson();
    this.photosForm = this.formBuilder.group({
      'itemId': [null, Validators.required],
      'path': [null, Validators.required],
    });
  }

  onFormSubmit(form: NgForm) {
    this.fs.postPhotos(form)
      .subscribe(res => {
        const id = res['key'];
        this.router.navigate(['/photos-details', id]);
      }, (err) => {
        console.log(err);
      });
  }

}
