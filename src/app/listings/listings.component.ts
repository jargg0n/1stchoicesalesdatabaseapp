import { Component, OnInit } from '@angular/core';

import { DataSource } from '@angular/cdk/collections';
import { FsService } from '../fs.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-listings',
  templateUrl: './listings.component.html',
  styleUrls: ['./listings.component.css']
})
export class ListingsComponent implements OnInit {

  displayedColumns = ['itemId', 'vendorId', 'ship', 'price', 'quantity', 'tax'];
  dataSource;

  itemDesc;

  listing;

  constructor(private fs: FsService) {
  }

  ngOnInit() {
    this.fs.getListings().subscribe((data: Array<Listing>) => {
      this.listing = data;
      this.dataSource = new MatTableDataSource(this.listing);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}

export class ListingDataSource extends DataSource<any> {

  constructor(private fs: FsService) {
    super();
  }

  connect() {
    return this.fs.getListings();
  }

  disconnect() {

  }
}

export interface Listing {
  itemId: String;
  vendorId: String;
  ship: String;
  price: String;
  quantity: String;
  tax: String;
}
