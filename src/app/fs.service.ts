import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';
import firestore from 'firebase/firestore';

@Injectable({
  providedIn: 'root'
})
export class FsService {

  sellerRef = firebase.firestore().collection('sellers');

  itemRef = firebase.firestore().collection('items');

  photoRef = firebase.firestore().collection('photos');

  vendorRef = firebase.firestore().collection('vendors');

  listingRef = firebase.firestore().collection('listings');

  constructor() { }

  getSellersJson(): any {
    const sellers = new Array();
    this.sellerRef.onSnapshot((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        const data = doc.data();
        sellers.push({
          key: doc.id,
          name: data.name,
          email: data.email,
          phone: data.phone,
          address: data.address,
          city: data.city,
          state: data.state,
          zip: data.zip,
          rating: data.rating
        });
      });
    });
    return sellers;
  }

  getSellers(): Observable<any> {
    return new Observable((observer) => {
      this.sellerRef.onSnapshot((querySnapshot) => {
        const sellers = [];
        querySnapshot.forEach((doc) => {
          const data = doc.data();
          sellers.push({
            key: doc.id,
            name: data.name,
            email: data.email,
            phone: data.phone,
            address: data.address,
            city: data.city,
            state: data.state,
            zip: data.zip,
            rating: data.rating
          });
        });
        observer.next(sellers);
      });
    });
  }

  getSeller(id: string): Observable<any> {
    return new Observable((observer) => {
      this.sellerRef.doc(id).get().then((doc) => {
        const data = doc.data();
        observer.next({
          key: doc.id,
          name: data.name,
          email: data.email,
          phone: data.phone,
          address: data.address,
          city: data.city,
          state: data.state,
          zip: data.zip,
          rating: data.rating
        });
      });
    });
  }

  postSellers(data): Observable<any> {
    return new Observable((observer) => {
      this.sellerRef.add(data).then((doc) => {
        observer.next({
          key: doc.id,
        });
      });
    });
  }

  updateSellers(id: string, data): Observable<any> {
    return new Observable((observer) => {
      this.sellerRef.doc(id).set(data).then(() => {
        observer.next();
      });
    });
  }

  deleteSellers(id: string): Observable<{}> {
    return new Observable((observer) => {
      this.sellerRef.doc(id).delete().then(() => {
        observer.next();
      });
    });
  }

  getItemsJson(): any {
    const items = new Array();
    this.itemRef.onSnapshot((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        const data = doc.data();
        items.push({
          key: doc.id,
          sellerId: data.sellerId,
          desc: data.desc,
          qoh: data.qoh,
          buy: data.buy,
          sell: data.sell,
          ship: data.ship,
          tax: data.tax
        });
      });
    });
    return items;
  }

  getItems(): Observable<any> {
    return new Observable((observer) => {
      this.itemRef.onSnapshot((querySnapshot) => {
        const items = [];
        querySnapshot.forEach((doc) => {
          const data = doc.data();
          items.push({
            key: doc.id,
            sellerId: data.sellerId,
            desc: data.desc,
            qoh: data.qoh,
            buy: data.buy,
            sell: data.sell,
            ship: data.ship,
            tax: data.tax
          });
        });
        observer.next(items);
      });
    });
  }

  getItem(id: string): Observable<any> {
    return new Observable((observer) => {
      this.itemRef.doc(id).get().then((doc) => {
        const data = doc.data();
        observer.next({
          key: doc.id,
          sellerId: data.sellerId,
          desc: data.desc,
          qoh: data.qoh,
          buy: data.buy,
          sell: data.sell,
          ship: data.ship,
          tax: data.tax
        });
      });
    });
  }

  postItems(data): Observable<any> {
    return new Observable((observer) => {
      this.itemRef.add(data).then((doc) => {
        observer.next({
          key: doc.id,
        });
      });
    });
  }

  updateItems(id: string, data): Observable<any> {
    return new Observable((observer) => {
      this.itemRef.doc(id).set(data).then(() => {
        observer.next();
      });
    });
  }

  deleteItems(id: string): Observable<{}> {
    return new Observable((observer) => {
      this.itemRef.doc(id).delete().then(() => {
        observer.next();
      });
    });
  }

  getPhotos(): Observable<any> {
    return new Observable((observer) => {
      this.photoRef.onSnapshot((querySnapshot) => {
        const photos = [];
        querySnapshot.forEach((doc) => {
          const data = doc.data();
          photos.push({
            key: doc.id,
            itemId: data.itemId,
            path: data.path
          });
        });
        observer.next(photos);
      });
    });
  }

  getPhoto(id: string): Observable<any> {
    return new Observable((observer) => {
      this.photoRef.doc(id).get().then((doc) => {
        const data = doc.data();
        observer.next({
          key: doc.id,
          itemId: data.itemId,
          path: data.path
        });
      });
    });
  }

  postPhotos(data): Observable<any> {
    return new Observable((observer) => {
      this.photoRef.add(data).then((doc) => {
        observer.next({
          key: doc.id
        });
      });
    });
  }

  updatePhotos(id: string, data): Observable<any> {
    return new Observable((observer) => {
      this.photoRef.doc(id).set(data).then(() => {
        observer.next();
      });
    });
  }

  deletePhotos(id: string): Observable<{}> {
    return new Observable((observer) => {
      this.photoRef.doc(id).delete().then(() => {
        observer.next();
      });
    });
  }

  getVendorsJson(): any {
    const vendors = new Array();
    this.vendorRef.onSnapshot((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        const data = doc.data();
        vendors.push({
          key: doc.id,
          name: data.name,
          saleCut: data.saleCut,
          listCut: data.listCut
        });
      });
    });
    return vendors;
  }

  getVendors(): Observable<any> {
    return new Observable((observer) => {
      this.vendorRef.onSnapshot((querySnapshot) => {
        const vendors = [];
        querySnapshot.forEach((doc) => {
          const data = doc.data();
          vendors.push({
            key: doc.id,
            name: data.name,
            saleCut: data.saleCut,
            listCut: data.listCut
          });
        });
        observer.next(vendors);
      });
    });
  }

  getVendor(id: string): Observable<any> {
    return new Observable((observer) => {
      this.vendorRef.doc(id).get().then((doc) => {
        const data = doc.data();
        observer.next({
          key: doc.id,
          name: data.name,
          saleCut: data.saleCut,
          listCut: data.listCut
        });
      });
    });
  }

  postVendors(data): Observable<any> {
    return new Observable((observer) => {
      this.vendorRef.add(data).then((doc) => {
        observer.next({
          key: doc.id,
        });
      });
    });
  }

  updateVendors(id: string, data): Observable<any> {
    return new Observable((observer) => {
      this.vendorRef.doc(id).set(data).then(() => {
        observer.next();
      });
    });
  }

  deleteVendors(id: string): Observable<{}> {
    return new Observable((observer) => {
      this.vendorRef.doc(id).delete().then(() => {
        observer.next();
      });
    });
  }

  getListings(): Observable<any> {
    return new Observable((observer) => {
      this.listingRef.onSnapshot((querySnapshot) => {
        const listings = [];
        querySnapshot.forEach((doc) => {
          const data = doc.data();
          listings.push({
            key: doc.id,
            itemId: data.itemId,
            vendorId: data.vendorId,
            ship: data.ship,
            price: data.price,
            quantity: data.quantity,
            tax: data.tax
          });
        });
        observer.next(listings);
      });
    });
  }

  getListing(id: string): Observable<any> {
    return new Observable((observer) => {
      this.listingRef.doc(id).get().then((doc) => {
        const data = doc.data();
        observer.next({
          key: doc.id,
          itemId: data.itemId,
          vendorId: data.vendorId,
          ship: data.ship,
          price: data.price,
          quantity: data.quantity,
          tax: data.tax
        });
      });
    });
  }

  postListings(data): Observable<any> {
    return new Observable((observer) => {
      this.listingRef.add(data).then((doc) => {
        observer.next({
          key: doc.id,
        });
      });
    });
  }

  updateListings(id: string, data): Observable<any> {
    return new Observable((observer) => {
      this.listingRef.doc(id).set(data).then(() => {
        observer.next();
      });
    });
  }

  deleteListings(id: string): Observable<{}> {
    return new Observable((observer) => {
      this.listingRef.doc(id).delete().then(() => {
        observer.next();
      });
    });
  }
}
