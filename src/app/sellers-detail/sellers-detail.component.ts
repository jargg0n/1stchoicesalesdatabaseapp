import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { FsService } from '../fs.service';

@Component({
  selector: 'app-sellers-detail',
  templateUrl: './sellers-detail.component.html',
  styleUrls: ['./sellers-detail.component.css']
})
export class SellersDetailComponent implements OnInit {

  seller = {};

  constructor(private route: ActivatedRoute, private router: Router, private fs: FsService) { }

  ngOnInit() {
    this.getSellerDetails(this.route.snapshot.params['id']);
  }

  getSellerDetails(id) {
    this.fs.getSeller(id)
      .subscribe(data => {
        console.log(data);
        this.seller = data;
      });
  }

  deleteSeller(id) {
    this.fs.deleteSellers(id)
      .subscribe(res => {
        this.router.navigate(['/sellers']);
      }, (err) => {
        console.log(err);
      }
      );
  }

  // deleteItems(id) {
  //   let items = new Array<Item>();
  //   items = this.fs.getItemsJson();
  //   for (let i = 0; i < items.length; i++) {
  //     if (items[i].sellerId === id) {
  //       this.fs.deleteItems(items[i].key + '').subscribe();
  //     }
  //   }
  // }

}

// export interface Item {
//   key: String;
//   sellerId: String;
//   desc: String;
//   qoh: String;
//   buy: String;
//   sell: String;
//   ship: String;
//   tax: String;
// }
