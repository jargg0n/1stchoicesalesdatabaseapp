import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { FsService } from '../fs.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';

@Component({
  selector: 'app-vendors-create',
  templateUrl: './vendors-create.component.html',
  styleUrls: ['./vendors-create.component.css']
})
export class VendorsCreateComponent implements OnInit {

  vendorsForm: FormGroup;

  constructor(private router: Router, private fs: FsService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.vendorsForm = this.formBuilder.group({
      'name': [null, Validators.required],
      'saleCut': [null, Validators.required],
      'listCut': [null, Validators.required],
    });
  }

  onFormSubmit(form: NgForm) {
    this.fs.postVendors(form)
      .subscribe(res => {
        const id = res['key'];
        this.router.navigate(['/vendors-details', id]);
      }, (err) => {
        console.log(err);
      });
  }

}

