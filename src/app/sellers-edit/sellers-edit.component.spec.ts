import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellersEditComponent } from './sellers-edit.component';

describe('SellersEditComponent', () => {
  let component: SellersEditComponent;
  let fixture: ComponentFixture<SellersEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellersEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellersEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
