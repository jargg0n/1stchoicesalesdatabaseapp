import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { FsService } from '../fs.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-sellers-edit',
  templateUrl: './sellers-edit.component.html',
  styleUrls: ['./sellers-edit.component.css']
})
export class SellersEditComponent implements OnInit {

  sellersForm: FormGroup;
  id = '';

  constructor(private router: Router, private route: ActivatedRoute, private fs: FsService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getSeller(this.route.snapshot.params['id']);
    this.sellersForm = this.formBuilder.group({
      'name': [null, Validators.required],
      'email': [null, Validators.required],
      'phone': [null, Validators.required],
      'address': [null, Validators.required],
      'city': [null, Validators.required],
      'state': [null, Validators.required],
      'zip': [null, Validators.required],
      'rating': [null, Validators.required]
    });
  }

  getSeller(id) {
    this.fs.getSeller(id).subscribe(data => {
      this.id = data.key;
      this.sellersForm.setValue({
        name: data.name,
        email: data.email,
        phone: data.phone,
        address: data.address,
        city: data.city,
        state: data.state,
        zip: data.zip,
        rating: data.rating
      });
    });
  }

  onFormSubmit(form: NgForm) {
    this.fs.updateSellers(this.id, form)
      .subscribe(res => {
        this.router.navigate(['/sellers']);
      }, (err) => {
        console.log(err);
      }
      );
  }

  sellersDetails() {
    this.router.navigate(['/sellers-details', this.id]);
  }

}
