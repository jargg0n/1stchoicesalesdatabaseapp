import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { FsService } from '../fs.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-listings-edit',
  templateUrl: './listings-edit.component.html',
  styleUrls: ['./listings-edit.component.css']
})
export class ListingsEditComponent implements OnInit {

  listingsForm: FormGroup;

  items;

  vendors;

  id = '';

  constructor(private router: Router, private route: ActivatedRoute, private fs: FsService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.items = this.fs.getItemsJson();
    this.vendors = this.fs.getVendorsJson();
    this.getListing(this.route.snapshot.params['id']);
    this.listingsForm = this.formBuilder.group({
      'itemId': [null, Validators.required],
      'vendorId': [null, Validators.required],
      'ship': [null, Validators.required],
      'price': [null, Validators.required],
      'quantity': [null, Validators.required],
      'tax': [null, Validators.required],
    });
  }

  getListing(id) {
    this.fs.getListing(id).subscribe(data => {
      this.id = data.key;
      this.listingsForm.setValue({
        itemId: data.itemId,
        vendorId: data.vendorId,
        ship: data.ship,
        price: data.price,
        quantity: data.quantity,
        tax: data.tax
      });
    });
  }

  onFormSubmit(form: NgForm) {
    this.fs.updateListings(this.id, form)
      .subscribe(res => {
        this.router.navigate(['/listings']);
      }, (err) => {
        console.log(err);
      }
      );
  }

  listingsDetails() {
    this.router.navigate(['/listings-details', this.id]);
  }

}

