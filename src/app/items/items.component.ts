import { Component, OnInit } from '@angular/core';

import { DataSource } from '@angular/cdk/collections';
import { FsService } from '../fs.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  displayedColumns = ['desc', 'buy', 'sell', 'qoh'];
  dataSource;

  item;

  constructor(private fs: FsService) {
  }

  ngOnInit() {
    this.fs.getItems().subscribe((data: Array<Item>) => {
      this.item = data;
      this.dataSource = new MatTableDataSource(this.item);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}

export class ItemDataSource extends DataSource<any> {

  filter;

  constructor(private fs: FsService) {
    super();
  }

  connect() {
    return this.fs.getItems();
  }

  disconnect() {

  }
}

export interface Item {
  sellerId: String;
  desc: String;
  qoh: String;
  buy: String;
  sell: String;
  ship: String;
  tax: String;
}
