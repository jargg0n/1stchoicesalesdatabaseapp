import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { FsService } from '../fs.service';

@Component({
  selector: 'app-items-detail',
  templateUrl: './items-detail.component.html',
  styleUrls: ['./items-detail.component.css']
})
export class ItemsDetailComponent implements OnInit {

  item = {};

  sellerName = 'SELLER DELETED';

  constructor(private route: ActivatedRoute, private router: Router, private fs: FsService) { }

  ngOnInit() {
    this.getItemDetails(this.route.snapshot.params['id']);
  }

  getItemDetails(id) {
    this.fs.getItem(id)
      .subscribe(data => {
        this.item = data;
        this.fs.getSeller(data['sellerId']).subscribe(dataa => {
          this.sellerName = dataa.name;
        });
      });
  }

  deleteItem(id) {
    this.fs.deleteItems(id)
      .subscribe(res => {
        this.router.navigate(['/items']);
      }, (err) => {
        console.log(err);
      }
      );
  }

}
