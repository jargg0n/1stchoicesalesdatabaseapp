import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { FsService } from '../fs.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';

@Component({
  selector: 'app-listings-create',
  templateUrl: './listings-create.component.html',
  styleUrls: ['./listings-create.component.css']
})
export class ListingsCreateComponent implements OnInit {

  items;

  vendors;

  listingsForm: FormGroup;

  constructor(private router: Router, private fs: FsService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.items = this.fs.getItemsJson();
    this.vendors = this.fs.getVendorsJson();
    this.listingsForm = this.formBuilder.group({
      'itemId': [null, Validators.required],
      'vendorId': [null, Validators.required],
      'ship': [null, Validators.required],
      'price': [null, Validators.required],
      'quantity': [null, Validators.required],
      'tax': [null, Validators.required],
    });
  }

  onFormSubmit(form: NgForm) {
    this.fs.postListings(form)
      .subscribe(res => {
        const id = res['key'];
        this.router.navigate(['/listings-details', id]);
      }, (err) => {
        console.log(err);
      });
  }

}

