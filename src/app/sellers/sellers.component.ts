import { Component, OnInit } from '@angular/core';

import { DataSource } from '@angular/cdk/collections';
import { FsService } from '../fs.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-sellers',
  templateUrl: './sellers.component.html',
  styleUrls: ['./sellers.component.css']
})
export class SellersComponent implements OnInit {

  displayedColumns = ['name', 'email'];
  dataSource;

  seller;

  constructor(private fs: FsService) {
  }

  ngOnInit() {
    this.fs.getSellers().subscribe((data: Array<Seller>) => {
      this.seller = data;
      this.dataSource = new MatTableDataSource(this.seller);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}

export interface Seller {
  name: String;
  email: String;
  phone: String;
  address: String;
  city: String;
  state: String;
  zip: String;
  rating: String;
}
